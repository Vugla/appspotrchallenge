//
//  ViewController.m
//  AppSpotrChallenge
//
//  Created by Predrag Samardzic on 03/09/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.label.text = @"Fetching array of words and searching for the non-duplicate one...";
    
    //preparing and sending GET request to fetch the array
    NSURL *url = [NSURL URLWithString:@"https://api2.appspotr.com/givemeachallenge"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [self.spinner startAnimating];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[[NSOperationQueue  alloc]init]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
       
         
         if (data.length > 0 && connectionError == nil)
         {
             
             
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                               options:0
                                                                 error:NULL];
             //extract words array
             NSArray* words = [result objectForKey:@"quiz"];
             
             //find the non-duplicate word
             NSString* oddDuck = [self findTheOddDuck:words];
             
             //update UI on main thread
             dispatch_async(dispatch_get_main_queue(), ^{
                 self.label.text = [NSString stringWithFormat:@"The Odd Duck is:\n\"%@\"", oddDuck];
                 [self.spinner stopAnimating];
             });
                 
         }else{
             //error alert
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self.spinner stopAnimating];
                  [self showMessage:@"Check your internet connection and try again." withTitle:@"An error occured"];
                  });
         }
         
         
     }];

    
    
}

//the most efficient method I came up with is using dictionary - its going through array only one time
-(NSString*)findTheOddDuck:(NSArray*)words{
    
    NSMutableDictionary* tempDictionary = [[NSMutableDictionary alloc]init];
    
    //
    for (NSString* word in words) {
        
        //if the word is already in the dictionary, means we found a duplicate, so we remove it
        if ([tempDictionary objectForKey:word]) {
            [tempDictionary removeObjectForKey:word];
            
        //if the word is not already in the dictionary, we put it in
        }else{
            [tempDictionary setObject:@"" forKey:word];
        }
    }
    
    //in the end only the non duplicate word remains in the dictionary
    return [[tempDictionary allKeys]objectAtIndex:0];
}


// Show an alert message
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
