//
//  AppDelegate.h
//  AppSpotrChallenge
//
//  Created by Predrag Samardzic on 03/09/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

